/* global ScrollReveal Chart $ */

$(function () {
  $('.navbar-collapse a, #footer').on('click', function (event) {
    event.preventDefault()
    var hash = this.hash

    $('body,html').animate({ scrollTop: $(hash).offset().top }, 1000, function () { window.location.hash = hash })
  })
})

// ScrollReveal Options

const sr = ScrollReveal()

sr.reveal('h5', {
  origin: 'top',
  duration: 1000
})

sr.reveal('h2', {
  origin: 'top',
  duration: 1000
})

sr.reveal('h1', {
  origin: 'top',
  duration: 1000
})

sr.reveal('h3', {
  origin: 'top',
  duration: 1000
})

sr.reveal('h4', {
  origin: 'top',
  duration: 1000
})

sr.reveal('#img-container', {
  origin: 'bottom',
  duration: 1400,
  reset: true,
  delay: 50,
  interval: 220
})

sr.reveal('p', {
  origin: 'top',
  duration: 1100
})

sr.reveal('.fa--scroll', {
  origin: 'left',
  duration: 1500,
  reset: true,
  interval: 200
})

sr.reveal('hr', {
  origin: 'left',
  duration: 2000,
  reset: true,
  delay: 150,
  interval: 250,
  distance: '500px'
})

sr.reveal('.skill-bar', {
  origin: 'left',
  duration: 1400,
  delay: 100,
  interval: 200
})

sr.reveal('.name', {
  origin: 'left',
  duration: 1100
})

sr.reveal('.status', {
  origin: 'right',
  duration: 1100,
  delay: 150
})

sr.reveal('#nav-icon', {
  origin: 'top',
  duration: 1600,
  reset: true
})

sr.reveal('#background-canvas', {
  origin: 'bottom',
  duration: 1400
})

sr.reveal('.mouse-icon', {
  origin: 'top',
  duration: 2200,
  distance: '280px',
  delay: 100
})

// Chart Js Options

let ctx = document.getElementById('myChart').getContext('2d')
let chart = new Chart(ctx, {
  // The type of chart we want to create
  type: 'horizontalBar',

  // The data for our dataset
  data: {
    labels: ['HTML 5', 'CSS 3', 'Bootstrap 4', 'JavaScript', 'PHP 7', 'Symfony 4', 'Angular 6', 'Ruby', 'ReactJs', 'Golang'],
    datasets: [{
      label: '% ',
      backgroundColor: 'rgba(16, 21, 41, 0.750)',
      borderColor: 'rgb(250, 250, 250)',
      hoverBorderColor: [
        '#EC4C37', // color for data at index 0
        '#0072B0',
        '#593E78',
        '#E9A043',
        '#7276A9',
        '#000000',
        '#CA0037',
        '#F73227',
        '#00D9FA',
        '#53d0db'

      ],
      fillColor: 0.3,
      borderWidth: '1.5',
      hoverBorderWidth: '3',
      data: [95, 95, 95, 75, 80, 80, 60, 55, 50, 45]
    }]
  },

  // Configuration options go here
  options: {
    legend: {
      labels: {
        fontColor: '#ffffff',
        fontSize: 17,
        fontFamily: 'Lato',
        fontStyle: 'bold'
      }
    },
    responsive: true,
    maintainAspectRatio: true,
    tooltips: {
      titleFontFamily: 'Lato',
      titleFontStyle: 'bold',
      bodyFontFamily: 'Lato',
      bodyFontStyle: 'bold',
      backgroundColor: '#131228',
      caretSize: 3,
      caretPadding: 5
    },
    scales: {
      yAxes: [{
        ticks: {
          fontColor: 'white',
          beginAtZero: true,
          fontSize: 15,
          fontFamily: 'Lato',
          fontStyle: 'bold',
          padding: 15
        },
        gridLines: {
          color: 'transparent'
        }
      }],
      xAxes: [{
        ticks: {
          fontColor: 'white',
          beginAtZero: true,
          fontSize: 15,
          fontFamily: 'Lato',
          fontStyle: 'bold',
          padding: 10
        },
        gridLines: {
          zeroLineColor: 'transparent'
        }
      }]
    }

  }
})
